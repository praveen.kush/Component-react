import React from "react";
import ReactDOM from "react-dom/client";
import { Provider } from "react-redux";
import store from "./redux/store";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router } from "react-router-dom";

const root = ReactDOM.createRoot(document.getElementById("root"));

root.render(
  <Provider store={store}>
    <Router>
      <App />
    </Router>
  </Provider>
);
// ReactDOM.render(
//   <React.StrictMode>
//     <Provider store={store}>
//     <Router>
//       <App />
//     </Router>
//     </Provider>
//   </React.StrictMode>,
//   document.getElementById('root')
// );

reportWebVitals();
