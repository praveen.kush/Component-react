import Home from "../src/component/Home/Home/Home";
import { Routes, Route } from "react-router-dom";
import React, { createContext, useEffect, useState } from "react";
import About from "./component/Home/About/About";
import Dashboard from "./component/Dashoboard/Dashboard/Dashboard";
import { getDecodedUser } from "./component/Login/LoginManager";
import LoginModal from "./component/Login/LoginModal";
import PrivateRoute from "./component/Login/PrivateRoute";
import NotFound from "./component/NotFound";
import Employee from "./component/Employee/Employee";
import EmployeeDetails from "./component/Employee/EmployeeDetails";
import Application from "./component/Shared/ThemeDrawer/ThemeDrawer";
import { useSelector, useDispatch } from "react-redux";

import "./App.css";

export const UserContext = createContext();

const App = () => {
  const { theme } = useSelector((state) => state.theme);

  const [admin, setAdmin] = useState(false);
  const [show, setShow] = useState(false);
  const [selectedService, setSelectedService] = useState({});
  const [user, setUser] = useState(getDecodedUser());

  const handleLoginModal = () => setShow(!show);
  return (
    <UserContext.Provider
      value={{
        user,
        setUser,
        admin,
        setAdmin,
        selectedService,
        setSelectedService,
        show,
        handleLoginModal,
      }}
    >
      <div  data-theme={theme}>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/about" element={<About />} />
          <Route path="/employee" element={<Employee />} />
          <Route path="/employee/:id" element={<EmployeeDetails />} />

          {/* <Route path="/login" element={<LoginModal />} /> */}
          <Route
            path="/dashboard/*"
            element={
              <PrivateRoute redirectTo="/login">
                <Dashboard />
              </PrivateRoute>
            }
          />
          <Route path="*" element={<NotFound />} />
        </Routes>
      </div>

      <Application />
    </UserContext.Provider>
  );
};

export default App;
