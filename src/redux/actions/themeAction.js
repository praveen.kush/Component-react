import { TOGGLE_DARK_MODE } from "../themeTypes";
export const toggleDarkMode = (theme) => {
  return {
    type: TOGGLE_DARK_MODE,
    payload:theme
  };
};
