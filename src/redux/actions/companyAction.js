import {
  FETCH_COMPANY_BY_ID_REQUEST,
  FETCH_COMPANY_BY_ID_SUCCESS,
  FETCH_COMPANY_BY_ID_FAILURE,
  UPDATE_COMPANY_REQUEST,
  UPDATE_COMPANY_SUCCESS,
  UPDATE_COMPANY_FAILURE,
} from "../companyTypes";
import axios from "axios";


const fetchCompanyByIdRequest = () => {
  return {
    type: FETCH_COMPANY_BY_ID_REQUEST,
  };
};
const fetchCompanyByIdSuccess = (company) => {
  return {
    type: FETCH_COMPANY_BY_ID_SUCCESS,
    payload: company,
  };
};
const fetchCompanyByIdFailure = (error) => {
  return {
    type: FETCH_COMPANY_BY_ID_FAILURE,
    payload: error,
  };
};

export const updateCompanyRequest = () => {
  return {
    type: UPDATE_COMPANY_REQUEST,
  };
};
const updateCompanySuccess = (companys) => {
  return {
    type: UPDATE_COMPANY_SUCCESS,
    payload: companys,
  };
};
const updateCompanyFailure = (error) => {
  return {
    type: UPDATE_COMPANY_FAILURE,
    payload: error,
  };
};

export const fetchCompanyById = (id) => {
  return function (dispatch) {
    dispatch(fetchCompanyByIdRequest());

    return axios
      .get(`http://localhost:3000/company/${id}`)
      .then((response) => {
        let array = [];
        const companys = response.data;
        array.push(companys);
        return dispatch(fetchCompanyByIdSuccess(array));
      })
      .catch((error) => {
        return dispatch(fetchCompanyByIdFailure(error.message));
      });
  };
};

export const updateCompanyById = (id, body) => {
  return function (dispatch) {
    dispatch(updateCompanyRequest());

    axios
      .put(`http://localhost:3000/company/${id}`, body)
      .then((response) => {
        const companys = response.data;
        dispatch(fetchCompanyRequest(companys));
      })
      .catch((error) => {
        dispatch(updateCompanyFailure(error.message));
      });
  };
};
