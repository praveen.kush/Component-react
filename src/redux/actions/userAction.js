import {
  FETCH_USERS_REQUESTS,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_FAILURE,
  FETCH_USER_BY_ID_REQUEST,
  FETCH_USER_BY_ID_SUCCESS,
  FETCH_USER_BY_ID_FAILURE,
  UPDATE_USER_REQUEST,
  UPDATE_USER_SUCCESS,
  UPDATE_USER_FAILURE,
} from "../userTypes";
import axios from "axios";

const fetchUserRequest = () => {
  return {
    type: FETCH_USERS_REQUESTS,
  };
};
const fetchUserSuccess = (users) => {
  return {
    type: FETCH_USERS_SUCCESS,
    payload: users,
  };
};
const fetchUserFailure = (error) => {
  return {
    type: FETCH_USERS_FAILURE,
    payload: error,
  };
};

const fetchUserByIdRequest = () => {
  return {
    type: FETCH_USER_BY_ID_REQUEST,
  };
};
const fetchUserByIdSuccess = (user) => {
  return {
    type: FETCH_USER_BY_ID_SUCCESS,
    payload: user,
  };
};
const fetchUserByIdFailure = (error) => {
  return {
    type: FETCH_USER_BY_ID_FAILURE,
    payload: error,
  };
};

export const updateUserRequest = () => {
  return {
    type: UPDATE_USER_REQUEST,
  };
};
const updateUserSuccess = (users) => {
  return {
    type: UPDATE_USER_SUCCESS,
    payload: users,
  };
};
const updateUserFailure = (error) => {
  return {
    type: UPDATE_USER_FAILURE,
    payload: error,
  };
};
export const fetchUsers = () => {
  return function (dispatch) {
    dispatch(fetchUserRequest());

    axios
      .get(`http://localhost:3000/users`)
      .then((response) => {
        const users = response.data;
        dispatch(fetchUserSuccess(users));
      })
      .catch((error) => {
        dispatch(fetchUserFailure(error.message));
      });
  };
};

export const fetchUserById = (id) => {
  return function (dispatch) {
    dispatch(fetchUserByIdRequest());

    return axios
      .get(`http://localhost:3000/users/${id}`)
      .then((response) => {
        let array = [];
        const users = response.data;
        array.push(users);
        return dispatch(fetchUserByIdSuccess(array));
      })
      .catch((error) => {
        return dispatch(fetchUserByIdFailure(error.message));
      });
  };
};

export const updateUserById = (id, body) => {
  return function (dispatch) {
    dispatch(updateUserRequest());

    axios
      .put(`http://localhost:3000/users/${id}`, body)
      .then((response) => {
        const users = response.data;
        dispatch(fetchUserRequest(users));
      })
      .catch((error) => {
        dispatch(updateUserFailure(error.message));
      });
  };
};
