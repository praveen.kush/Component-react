import { TOGGLE_DARK_MODE } from "../themeTypes";
const initialState = {
  theme: "light",
};

export const themeReducer = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_DARK_MODE:
      return { ...state, theme: action.payload };
    default:
      return state;
  }
};
