import {
  FETCH_USERS_REQUESTS,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_FAILURE,
  FETCH_USER_BY_ID_REQUEST,
  FETCH_USER_BY_ID_SUCCESS,
  FETCH_USER_BY_ID_FAILURE,
  UPDATE_USER_REQUEST,
  UPDATE_USER_SUCCESS,
  UPDATE_USER_FAILURE 
} from "../userTypes";
const initialState = {
  loading: false,
  users: [],
  userProfile:[],
  error: "",
};
export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_USERS_REQUESTS:
      return {
        ...state,
        loading: true,
      };
    case FETCH_USERS_SUCCESS:
      return {
        ...state,
        loading: false,
        users:action.payload,
        error: "",
      };
    case FETCH_USERS_FAILURE:
      return {
        ...state,
        loading: false,
        users: [],
        error: action.payload,
      };




      case FETCH_USER_BY_ID_REQUEST:
        return {
          ...state,
          loading: true,
        };
      case FETCH_USER_BY_ID_SUCCESS:
        return {
          ...state,
          loading: false,
          userProfile:action.payload,
          error: "",
        };
      case FETCH_USER_BY_ID_FAILURE:
        return {
          ...state,
          loading: false,
          users: [],
          error: action.payload,
        };



      case UPDATE_USER_REQUEST:
        return {
          ...state,
          loading: true,
        };
      case UPDATE_USER_SUCCESS:
        return {
          ...state,
          loading: false,
          users:action.payload,
          error: "",
        };
      case UPDATE_USER_FAILURE :
        return {
          ...state,
          loading: false,
          users: [],
          error: action.payload,
        };
    default:
      return state;
  }
};
