import {
  FETCH_COMPANY_BY_ID_REQUEST,
  FETCH_COMPANY_BY_ID_SUCCESS,
  FETCH_COMPANY_BY_ID_FAILURE,
  UPDATE_COMPANY_REQUEST,
  UPDATE_COMPANY_SUCCESS,
  UPDATE_COMPANY_FAILURE,
} from "../companyTypes";
const initialState = {
  loading: false,
  companyProfile: [],
  error: "",
};
export const companyReducer = (state = initialState, action) => {
  switch (action.type) {
   case FETCH_COMPANY_BY_ID_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case FETCH_COMPANY_BY_ID_SUCCESS:
      return {
        ...state,
        loading: false,
        companyProfile: action.payload,
        error: "",
      };
    case FETCH_COMPANY_BY_ID_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case UPDATE_COMPANY_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case UPDATE_COMPANY_SUCCESS:
      return {
        ...state,
        loading: false,
        companys: action.payload,
        error: "",
      };
    case UPDATE_COMPANY_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};
