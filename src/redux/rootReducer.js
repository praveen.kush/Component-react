import {combineReducers} from 'redux'
import {themeReducer, userReducer,companyReducer} from './reducers'
const rootReducer = combineReducers({
    theme: themeReducer,
    user:userReducer,
    company:companyReducer
})
export default rootReducer