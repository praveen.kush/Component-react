import React, { useState } from "react";
import { Row, Col } from "react-bootstrap";
import {
  ShareFill,
  PhoneFill,
  XCircleFill,
  Bookmark,
} from "react-bootstrap-icons";
import { useNavigate } from "react-router-dom";

import EmployeeActions from "./EmployeeActions";
import SharingOptions from "./SharingOptions";
import Animation from "./Animation";
import "./Employe.css";

function EmployeeFeed({ employee }) {
  const [show, setShow] = useState(false);
  const [share, setShare] = useState(false);
  let navigate = useNavigate();

  const handleEdit = () => {
    //TODO: add logic to edit
  };
  const handleDelete = () => {
    //TODO: add logic to delete
  };
  const handleShare = () => {
    //TODO: add logic to share
    setShare(!share);
  };
  return (
    <div className="empcontainer mx-auto mb-5 border rounded p-3 justify-item-center" onDoubleClick={() => navigate(`/employee/${employee?.id}`)}>
      <div>
        <Row style={{ opacity: show ? 0.6 : 1 }}>
          <Col
            xs={{ order: "first", span: 4 }}
            sm={{ order: "first", span: 2 }}
            md={2}
          >
            <img
              style={{ borderRadius: "50%", height: "80px", width: "100px" }}
              src={employee?.profile}
              alt="pic"
            />
          </Col>
          <Col
            xs={{ order: "last", span: 12 }}
            sm={{ order: "second", span: 5 }}
            md={7}
          >
            <h3 className="name">{employee?.firstName} {employee?.lastName}</h3>
            <h6 className="muted"> {employee?.designation} </h6>
          </Col>
          <Col
            xs={{ order: "second", span: 4, offset: 2 }}
            sm={{ order: "last", span: 3, offset: 0 }}
            md={2}
          >
            <img
              style={{ borderRadius: "50%", height: "80px", width: "100px" }}
              src={employee?.company_logo}
              title={employee?.company}
              alt="Company Logo"
            />

            <h6 className="text-center">{employee?.company} </h6>
          </Col>
          <Col
            xs={{ order: "second", span: 2 }}
            sm={{ order: "last", span: 2, offset: 0 }}
            md={1}
          >
            <EmployeeActions
              id={employee.id}
              handleDelete={handleDelete}
              handleEdit={handleEdit}
              handleShare={handleShare}
            />
          </Col>
        </Row>
        <hr />
        <div style={{ opacity: show ? 0.6 : 1 }}>
          <Row>
            <Col xs={4} sm={3} md={2} lg={2}>
              <p className="name">Skills</p>
            </Col>
            <Col xs={8} sm={9} md={10} lg={10}>
              {employee?.skills?.map((skill) => (
                <button className="skillBtn" key={skill?.id}>{skill?.text}</button>
              ))}
            </Col>
          </Row>
          <Row>
            <Col xs={4} sm={3} md={2} lg={2}>
              <p className="name">Location</p>
            </Col>
            <Col xs={8} sm={3} md={4} lg={4}>
              {employee?.location}
            </Col>
            <Col xs={4} sm={3} md={2} lg={2}>
              <p className="name">Experience</p>
            </Col>
            <Col xs={8} sm={3} md={4} lg={4}>
              {employee?.experience}
            </Col>
          </Row>
          <Row>
            <Col xs={4} sm={3} md={2} lg={2}>
              <p className="name">Duration</p>
            </Col>
            <Col xs={8} sm={3} md={4} lg={4}>
              {employee?.duration}
            </Col>
            <Col xs={4} sm={3} md={2} lg={2}>
              <p className="name">Status</p>
            </Col>
            <Col xs={8} sm={3} md={4} lg={4}>
              {employee?.status}
            </Col>
          </Row>
        </div>
        <hr />
        {show && share && (
          <Animation visible={share}>
            <XCircleFill
              className="m-2 icon"
              size={20}
              onClick={() => {
                handleShare();
                setShow(false);
              }}
            />
            <SharingOptions />
          </Animation>
        )}

        {!show && share && (
          <Animation visible={share}>
            <XCircleFill className="m-2 icon" size={20} onClick={handleShare} />
            <SharingOptions />
          </Animation>
        )}
        {!show && !share && (
          <Row>
            <Col xs={3} sm={4}>
              <Bookmark size="20px" />
            </Col>

            <Col xs={5} sm={6}>
              <PhoneFill /> {employee?.phone}
            </Col>

            <Col xs={4} sm={2}>
              <div className="show">
                <div className="shareicon">
                  <div style={{ marginLeft: "5%" }} className="showicon">
                    <ShareFill onClick={handleShare} />
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        )}
      </div>
    </div>
  );
}

export default EmployeeFeed;
