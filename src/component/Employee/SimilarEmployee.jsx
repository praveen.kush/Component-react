import React from "react";
import { Row, Col } from "react-bootstrap";
import EmployeeData from "../EmployeeData";
function SimilarEmployee() {
  return (
    <>
      <h5>Similar Profiles </h5>
      {EmployeeData?.map((employee) => {
        return (
          <Row className="p-1  align-items-center">
            <Col xs={3} sm={2} md={4}>
              <img
                style={{ borderRadius: "50%", height: "50px", width: "50px" }}
                src={employee?.profile}
                alt="pic"
              />
            </Col> 
            <Col xs= {9} sm={10} md={8}>
              <p>{employee?.firstName}{employee?.lastName}</p>
            </Col>
          </Row>
        );
      })}
    </>
  );
}

export default SimilarEmployee;
