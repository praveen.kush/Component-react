import React from "react";
import PDF from "./sample.pdf";
// Import Worker
import { Worker } from "@react-pdf-viewer/core";
// Import the main Viewer component
import { Viewer } from "@react-pdf-viewer/core";
// Import the styles
import "@react-pdf-viewer/core/lib/styles/index.css";
// default layout plugin
import { defaultLayoutPlugin } from "@react-pdf-viewer/default-layout";
// Import styles of default layout plugin
import "@react-pdf-viewer/default-layout/lib/styles/index.css";
//import EmployeePdf from "./EmployeePdf";
import './Employe.css'
function EmployeePdf() {
  // creating new plugin instance
  const defaultLayoutPluginInstance = defaultLayoutPlugin();
  return (
    <div
      style={{
        border: "1px solid rgba(0, 0, 0, 0.3)",
        height: "750px",
      }}
    >
      <Worker workerUrl="https://unpkg.com/pdfjs-dist@2.6.347/build/pdf.worker.min.js">
        <Viewer fileUrl={PDF} plugins={[defaultLayoutPluginInstance]}></Viewer>
      </Worker>
    </div>
  );
}

export default EmployeePdf;
