import React from "react";
import { Row, Col } from "react-bootstrap";
import {
  FacebookShareButton,
  TwitterShareButton,
  EmailShareButton,
  LinkedinShareButton,
  LinkedinIcon,
  FacebookIcon,
  TwitterIcon,
  EmailIcon,
} from "react-share";
import "./Employe.css";
function SharingOptions({size=3}) {
  return (
    <Row className={size == 3 ? 'p-3 m-auto':''}>
      <Col xs={size}>
        <FacebookShareButton url={"https://peing.net/ja/"}>
          <FacebookIcon size={"2rem"} round />
        </FacebookShareButton>
      </Col>
      <Col xs={size}>
        <TwitterShareButton
          url={"https://peing.net/ja/"}
          hashtags={["hashtag1", "hashtag2"]}
        >
          <TwitterIcon size={"2rem"} round />
        </TwitterShareButton>
      </Col>
      <Col xs={size}>
        <EmailShareButton url={"https://peing.net/ja/"}>
          <EmailIcon size={"2rem"} round />
        </EmailShareButton>
      </Col>
      <Col xs={size}>
        <LinkedinShareButton url={"https://peing.net/ja/"}>
          <LinkedinIcon size={"2rem"} round />
        </LinkedinShareButton>
      </Col>
    </Row>
  );
}

export default SharingOptions;
