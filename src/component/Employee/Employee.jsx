import React, { useEffect, useRef, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Spinner } from "react-bootstrap";
import EmployeeList from "./EmployeeFeed";
import { fetchUsers } from "../../redux/actions/userAction";
import "./Employe.css";
function Employee() {
  const [limit, setLimit] = useState(2);
  const { users: employeeData, loading } = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const myRef = useRef(null);

  //Auto Scroll on load more
  const setScroll = () => {
    myRef?.current?.scrollIntoView({ behavior: "auto", block: "end" });
  };
  useEffect(() => {
    dispatch(fetchUsers());
  }, []);
  useEffect(() => {
    setScroll();
  }, [limit]);
  const [y, setY] = useState(window.scrollY);

  useEffect(() => {
    window.addEventListener("scroll", (e) => handleNavigation(e));
  
    return () => { // return a cleanup function to unregister our function since its gonna run multiple times
      window.removeEventListener("scroll", (e) => handleNavigation(e));
    };
  }, [y]);

  const handleNavigation=()=>{ setLimit((oldLimit) => oldLimit + 2);}
  return (
    <>
      <div className="main_container" >
        <br />
        {employeeData?.slice(0, limit).map((employee, index) => {
          return (
            <>
              {index === limit - 1 && <div ref={myRef} />}
              <EmployeeList employee={employee} key={employee.id} />
            </>
          );
        })}

        <button
          className="iBtn load_moreBtn"
          onClick={() => {
            setLimit((oldLimit) => oldLimit + 2);
          }}
        >
          {loading ? (
            <Spinner animation="border" role="status">
              <span className="visually-hidden">Loading...</span>
            </Spinner>
          ) : (
            <>Load More</>
          )}
        </button>
      </div>
    </>
  );
}

export default Employee;
