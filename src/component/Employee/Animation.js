import React from "react";
import { motion } from "framer-motion";

function Animation({visible, children}) {
  return (
    <>
      <motion.div
        initial={{ opacity: 0 }}
        animate={{
          x: visible ? 10 : -10,
          opacity: visible ? 1 : 0,
        }}
        transition={{ duration: 1 }}
        className="employee-action"
      >
      {children}
      </motion.div>
    </>
  );
}

export default Animation;
