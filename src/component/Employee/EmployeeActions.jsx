import React from "react";
import { Dropdown } from "react-bootstrap";
import { ThreeDotsVertical } from "react-bootstrap-icons";
import { useNavigate } from "react-router-dom";

import "./Employe.css";
function EmployeeActions(props) {
  let navigate = useNavigate();
  const { id, handleDelete, handleEdit, handleShare } = props;
  return (
    <div className="mb-2">
      <Dropdown>
        <Dropdown.Toggle>
          <ThreeDotsVertical />
        </Dropdown.Toggle>

        <Dropdown.Menu variant="dark">
          <Dropdown.Item href="#/action-1" onClick={handleEdit}>
            Edit{" "}
          </Dropdown.Item>
          <Dropdown.Item href="#/action-2" onClick={handleDelete}>
            Delete{" "}
          </Dropdown.Item>
          <Dropdown.Item href="#/action-3" onClick={handleShare}>
            Share{" "}
          </Dropdown.Item>
          <Dropdown.Divider />
          <Dropdown.Item onClick={() => navigate(`/employee/${id}`)}>
            View Details{" "}
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    </div>
  );
}

export default EmployeeActions;
