import React from "react";
import { useSelector, useDispatch } from "react-redux";

import { Row, Col, Button } from "react-bootstrap";
import {
  ArrowLeftCircle,
  BookHalf,
  PencilSquare,
  Globe,
} from "react-bootstrap-icons";
import { useParams, useNavigate } from "react-router-dom";
import EmployeePdf from "./EmployeePdf";
import SharingOptions from "./SharingOptions";
import { useEffect } from "react";
import SimilarEmployee from "./SimilarEmployee";
import { fetchUserById } from "../../redux/actions/userAction";

function EmployeeDetails() {
  let { id } = useParams();
  let navigate = useNavigate();
  const { users: employee, loading } = useSelector((state) => state.user);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchUserById(id));
  }, []);
  return (
    <>
      {loading ? (
        "Loading..."
      ) : (
        <>
          <div className="mt-4 mx-5" onClick={() => navigate("/employee")}>
            <ArrowLeftCircle size="20px" /> Back To Feed
          </div>
          <div
            style={{ width: "90%" }}
            className="empcontainer mx-auto mt-5  border rounded p-3 justify-item-center"
          >
            <Row>
              <Col xs={4} md={2}>
                <img
                  style={{
                    borderRadius: "50%",
                    height: "150px",
                    width: "150px",
                  }}
                  src={employee[0]?.profile}
                  alt="pic"
                />
              </Col>
              <Col xs={8} md={4}>
                <h3>
                  {employee[0]?.firstName} {employee[0]?.lastName}{" "}
                </h3>
                <h6 className="muted"> {employee[0]?.designation} </h6>
                <Button variant="success">Contact</Button>
                <Button variant="light">Resume</Button>
              </Col>
              <Col className="p-4 p-md-0">
                <Row>
                  <Col xs={6} md={4}>
                    <p className="fw-bold">Availablity</p>
                    <p className="fw-bold">Status</p>
                    <p className="fw-bold">Location</p>
                    <p className="fw-bold">Experience</p>
                  </Col>
                  <Col xs={6} md={8}>
                    <p>{employee[0]?.location}</p>
                    <p>{employee[0]?.status}</p>
                    <p>{employee[0]?.location}</p>
                    <p>{employee[0]?.experience}</p>
                  </Col>
                  <Col md={12}>
                    <SharingOptions size={2} />
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
          <div
            style={{ width: "90%" }}
            className="mx-auto mt-3 p-3 justify-item-center"
          >
            <Row className="p-2">
              <Col
                sm={{ order: "second" }}
                md={{ order: "first" }}
                className="border m-sm-1 m-md-0 "
              >
                <h5>Websites</h5>

                <p>
                  <Globe /> Website
                </p>
                <p>
                  <PencilSquare /> Blog
                </p>
                <p>
                  <BookHalf /> Portfolio
                </p>
              </Col>
              <Col sm={{ order: "first" }} md={8}>
                <EmployeePdf />
              </Col>
              <Col sm={{ order: "last" }} md={2} className="border">
                <SimilarEmployee />
              </Col>
            </Row>
          </div>
        </>
      )}{" "}
    </>
  );
}

export default EmployeeDetails;
