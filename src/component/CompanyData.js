const CompanyData=[
    {
        id:1,
        companyName:'Gateway group of companies',
        foundation:"23rd feb, 1997",
        companyEmail:"gatewayCorp@gmail.com",
        regName:"Gateway Technolabs",
        managerName:"Kat Christian",
        strength:"900",
        location:'Ahmedabad, India',
        contact:9000298367
    }
]
export default CompanyData