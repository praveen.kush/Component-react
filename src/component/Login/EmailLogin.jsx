import React, {useContext, useState} from 'react';
import { useForm } from 'react-hook-form';
import { loginWithEmail } from './LoginManager';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope, faLock } from '@fortawesome/free-solid-svg-icons';
import toast from 'react-hot-toast';
import SocialMedia from './SocialMedia';
import { UserContext } from "../../App";


const EmailLogin = ({handleResponse, isOTP, setIsOTP}) => {
    const { register, handleSubmit, formState: { errors } } = useForm();
    const { handleLoginModal } = useContext(UserContext);

    const onSubmit = ({email, password}) => {
        const loading = toast.loading('Please wait...');
        loginWithEmail(email, password)
        .then(res => {
            if(res.error){
                toast.dismiss(loading);
                toast.error(res.error)
            }
            handleResponse(res)
            handleLoginModal()
            toast.dismiss(loading);
        })
    }
    return (
        <form onSubmit={handleSubmit(onSubmit)} className="sign-in-form">
        <h2 className="title">Sign in</h2>

        <>
          <div className="input-field">
            <span className="fIcon">
              <FontAwesomeIcon icon={faEnvelope} />
            </span>
            <input
              placeholder="Email"
              {...register("email", { required: true })}
            />
          </div>
          {errors.email && (
            <span className="text-warning">This field is required</span>
          )}
          <div class="input-field">
            <span className="fIcon">
              <FontAwesomeIcon icon={faLock} />
            </span>
            <input
              type="password"
              placeholder="Password"
              {...register("password", { required: true })}
            />
          </div>
          {errors.password && (
            <span className="text-warning">This field is required</span>
          )}
        </>

        <input className="iBtn" type="submit" value="Sign In" />

        <input
          className="iBtn"
          type="button"
          onClick={() => setIsOTP(!isOTP)}
          value={
            isOTP ? "Login With Email Address" : "Login With Mobile Number"
          }
        />
        <p className="social-text">Or Sign in with social platforms</p>
        <SocialMedia handleResponse={handleResponse} />
      </form>
    );
};

export default EmailLogin;