import React, { useContext, useState } from "react";
import OtpInput from "react-otp-input";
import { faLock, faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Button, Spinner } from "react-bootstrap";
import toast, { Toaster } from "react-hot-toast";
import { UserContext } from "../../../App";

function VerifyOtp({ prevStep, formData, setFormData }) {
  const { handleLoginModal } = useContext(UserContext);

  const [otp, setOtp] = useState();
  const [counter, setCounter] = React.useState(30);
  const [error, setError] = useState();
  const [editable, setIsEditable] = useState(false);
  const [loading, setLoading] = useState(false);

  // Timer for countdown
  React.useEffect(() => {
    const timer =
      counter > 0 && setInterval(() => setCounter(counter - 1), 1000);
    return () => clearInterval(timer);
  }, [counter]);

  const onSubmit = (e) => {
    e.preventDefault();
    if (formData?.mobile?.length !== 10) {
      setIsEditable(true);
      setError("Only 10 digits are allowed");
    } else if (otp?.length !== 4) {
      setError("Please enter the OTP");
    } else {
      //TODO: Add number change logic and sending otp again
      setIsEditable(false);
      setError();
    }
  };
  const verifyOTP = () => {
    //TODO: Add Logic to verify the OTP
    setLoading(true);

    //If OTP is verified
    handleLoginModal();
  };

  return (
    <>
      <form className="sign-in-form" style={{ top: "90%" }} onSubmit={onSubmit}>
        <div>
          <Toaster />
        </div>
        <span className="pageBackBtn" onClick={prevStep}>
          <FontAwesomeIcon icon={faArrowLeft} />
        </span>
        <span className="fIcon">
          <FontAwesomeIcon icon={faLock} />
        </span>
        <h2 className="title">Verification</h2>
        <p style={{ textAlign: "center" }}>
          Please check the OTP sent to your mobile number
        </p>
        <div style={{ display: "flex", justifyContent: "center" }}>
          <input
            className="editable-field"
            type="number"
            value={formData?.mobile}
            disabled={!editable}
            onChange={(e) => setFormData({ mobile: e.target.value })}
          />
          {editable ? (
            <button
              key="submit"
              style={{ border: "none", outline: "none" }}
              type="submit"
            >
              Done
            </button>
          ) : (
            <button
              key="edit"
              style={{ border: "none", outline: "none" }}
              type="button"
              onClick={() => {
                setIsEditable(!editable);
              }}
            >
              ✏️
            </button>
          )}
        </div>
        {error?.length > 0 && <span className="text-warning">{error}</span>}
        <br />
        <OtpInput
          isInputNum
          separator={
            <span>
              <strong>.</strong>
            </span>
          }
          value={otp}
          onChange={(otp) => setOtp(otp)}
          inputStyle={{
            width: "3rem",
            height: "3rem",
            margin: "0 1rem",
            fontSize: "2rem",
            borderRadius: 4,
            border: "1px solid rgba(0,0,0,0.3)",
          }}
          hasErrored={otp === "1234" || undefined ? false : true} // to check whether entered OTP is true or not
          errorStyle="error"
        />
        <br />
        {counter > 0 && (
          <Button variant="secendory">Resending OTP in 00:{counter}</Button>
        )}

        <input
          className="iBtn"
          disabled={counter}
          type="button"
          onClick={() => {
            //TODO: Add logic to resend the OTP
            toast.success("OTP sent successfully");
            setCounter(30);
          }}
          value="Resend OTP"
        />
        <button key="verify" className="iBtn" type="button" onClick={verifyOTP}>
          {loading ? (
            <Spinner animation="border" role="status">
              <span className="visually-hidden">Loading...</span>
            </Spinner>
          ) : (
            "Verify and Continue"
          )}
        </button>
      </form>
    </>
  );
}

export default VerifyOtp;
