import React from "react";
import { useForm } from "react-hook-form";
import { faMobile } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";


export default function MobileLogin({ nextStep, isOTP, setIsOTP, formData }) {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const onSubmit = (data) => {
    //TODO: call OTP verification service
    nextStep(data);
  };
  return (
    <form onSubmit={handleSubmit(onSubmit)} className="sign-in-form">
      <h2 className="title">Sign in</h2>
      <div className="input-field">
        <span className="fIcon">
          <FontAwesomeIcon icon={faMobile} />
        </span>
        <input
          type="number"
          placeholder="Mobile number"
          defaultValue={formData?.mobile}
          {...register("mobile", {
            required: true,
            minLength: 10,
            maxLength: 10,
          })}
        />
      </div>
      {errors.mobile && (
        <span className="text-warning">{errors?.mobile?.type =='minLength' || 'maxLength' ? 'Only 10 Digits are allowed':'This field is required'}</span>
      )}
     
      <input
        className="iBtn"
        type="button"
        onClick={() => setIsOTP(!isOTP)}
        value={isOTP ? "Login With Email Address" : "Login With Mobile Number"}
      />
       <input value="Continue" className="iBtn" type="submit" />
    </form>
  );
}
