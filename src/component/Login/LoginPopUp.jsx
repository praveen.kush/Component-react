import React, { useContext } from "react";
import { Button, Modal } from "react-bootstrap";
import { UserContext } from "../../App";
import LoginModal from "./LoginModal";
// import SignInForm from "./SignInForm";

function LoginPopUp() {
  const { show, handleLoginModal } = useContext(UserContext);
  return (
    <>
      <Modal
        show={show}
        onHide={handleLoginModal}
        backdrop="static"
        keyboard={false}
        size="lg"
      >
        <Modal.Header closeButton>
          <Modal.Title>Login/Sign Up</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <LoginModal />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleLoginModal}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default LoginPopUp;
