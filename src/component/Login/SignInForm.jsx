import React, { useState } from "react";
import VerifyOtp from "./MobileLogin/VerifyOtp";
import MobileLogin from "./MobileLogin/MobileLogin";
import EmailLogin from './EmailLogin'

const SignInForm = ({ handleResponse }) => {
 
  const [isOTP, setIsOTP] = useState(true);
  const [step, setstep] = useState(1);
  const [formData, setFormData] = useState();

  // function for going to next step by increasing step state by 1
  const nextStep = (data) => {
    setFormData(data)
    setstep(step + 1);
  };

  // function for going to previous step by decreasing step state by 1
  const prevStep = () => {
    setstep(step - 1);
  };


  return (
    <>
      {isOTP && step === 1 && (
        <MobileLogin
          nextStep={nextStep}
          prevStep={prevStep}
          isOTP={isOTP}
          setIsOTP={setIsOTP}
          formData={formData}
        />
      )}
      {isOTP && step === 2 && <VerifyOtp prevStep={prevStep} formData={formData} setFormData={setFormData}/>}
      
      {/* comment the below line to disable email login */}
      {!isOTP && <EmailLogin handleResponse={handleResponse} isOTP={isOTP} setIsOTP={setIsOTP}/>}
    </>
  );
};

export default SignInForm;
