import * as React from "react";
// Import Worker
import { Worker } from "@react-pdf-viewer/core";
// Import the main Viewer component
import { Viewer } from "@react-pdf-viewer/core";
// Import the styles
import "@react-pdf-viewer/core/lib/styles/index.css";
import { defaultLayoutPlugin } from "@react-pdf-viewer/default-layout";
import { Button , Paper} from "@mui/material";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";

const PreviewPdf = ({ url, open, handleClose }) => {
  const defaultLayoutPluginInstance = defaultLayoutPlugin();

  return (
    <>
      
        <Dialog
          open={open}
          onClose={handleClose}
          fullWidth="true"
          maxWidth="md"
        >
          <DialogTitle>Preview</DialogTitle>
          <div style={{ overflow: "hidden", height: "100%", width: "100%" }}>
            <div
              style={{
                border: "1px solid rgba(0, 0, 0, 0.3)",
                height: "750px",
              }}
            >
              <Worker workerUrl="https://unpkg.com/pdfjs-dist@2.6.347/build/pdf.worker.min.js">
                <Viewer
                  fileUrl={url}
                  plugins={[defaultLayoutPluginInstance]}
                ></Viewer>
              </Worker>
            </div>
          </div>
          <DialogActions>
          <Button onClick={handleClose}>Cancel</Button>
        </DialogActions>
        </Dialog>
  
    </>
  );
};
export default PreviewPdf;
