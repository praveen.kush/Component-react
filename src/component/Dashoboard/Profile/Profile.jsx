import { useState } from "react";
import { Box, Container, Grid } from "@mui/material";
import { AccountProfileDetails } from "./AccoutProfileDetails";
import { Otherprofiles } from "./OtherProfiles";

const Profile = () => {
  const [user, selectedUser] = useState(1);
  return (
    <>
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          py: 1,
        }}
      >
        <Container maxWidth="lg" className="p-1">
          <Grid container spacing={2}>
            <Grid item lg={3} md={6} xs={12}>
              <Otherprofiles selectedUser={selectedUser} />
            </Grid>
            <Grid item lg={9} md={6} xs={12}>
              <AccountProfileDetails user={user} />
            </Grid>
          </Grid>
        </Container>
      </Box>
    </>
  );
};

export default Profile;
