import React, { useEffect, useState } from "react";
import {
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  Paper,
} from "@mui/material";
import { useSelector, useDispatch } from "react-redux";
import { styled } from "@mui/material/styles";
import SkillSet from "../../Shared/SkillSet/SkillSet";
import FileUploadIcon from "@mui/icons-material/FileUpload";
import PictureAsPdfIcon from "@mui/icons-material/PictureAsPdf";
import AccountProfile from "../../Shared/AccountProfile";
import PreviewPdf from "./PreviewPdf";
import ActionButtons from "../../Shared/ActionButtons";
import Controls from "../../controls/Controls";
import { useForm, Form } from "../../custom hooks/useForm";
import {
  fetchUserById,
  updateUserById,
} from "../../../redux/actions/userAction";

export const AccountProfileDetails = ({ user }) => {
  const [open, setOpen] = React.useState(false);
  const [url, setUrl] = React.useState("");
  const [fileName, setFileName] = useState("");
  const dispatch = useDispatch();

  const { userProfile: employeeData, loading } = useSelector(
    (state) => state.user
  );
  let temp = {};
  const validate = (fieldValues = values) => {
    temp = { ...errors };
    if ("firstName" in fieldValues)
      temp.firstName = fieldValues.firstName ? "" : "This field is required.";
    if ("lastName" in fieldValues)
      temp.lastName = fieldValues.lastName ? "" : "This field is required.";
    if ("status" in fieldValues)
      temp.status = fieldValues.status ? "" : "This field is required.";
    if ("experience" in fieldValues)
      temp.experience = fieldValues.experience ? "" : "This field is required.";
    if ("location" in fieldValues)
      temp.location = fieldValues.location ? "" : "This field is required.";
    if ("designation" in fieldValues)
      temp.designation = fieldValues.designation
        ? ""
        : "This field is required.";
    if ("email" in fieldValues)
      temp.email = /$^|.+@.+..+/.test(fieldValues.email)
        ? fieldValues.email
          ? ""
          : "Email is required"
        : "Email is not valid.";

    if ("phone" in fieldValues)
      temp.phone =
        fieldValues.phone.toString().length > 9
          ? ""
          : "Minimum 10 numbers required.";
    setErrors({
      ...temp,
    });

    if (fieldValues == values) return Object.values(temp).every((x) => x == "");
  };
  const {
    values,
    setValues,
    isError,
    setIsError,
    errors,
    setErrors,
    handleChange,
    isDisable,
    setIsDisable,
    handleAddition,
    handleDrag,
    handleDelete,
  } = useForm(true, validate);
  useEffect(() => {
    if (
      Object.keys(errors).length === 0 ||
      Object.values(errors).every((x) => x == "")
    ) {
      setIsError(false);
    } else {
      setIsError(true);
    }
  }, [errors]);

  useEffect(() => {
    dispatch(fetchUserById(user));
  }, [user]);

  useEffect(() => {
    setValues(employeeData[0]);
  }, [employeeData]);

  const onChange = (e) => {
    const files = e.target.files;
    files.length > 0 && setUrl(URL.createObjectURL(files[0]));
    files.length > 0 && setFileName(files[0]?.name);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (validate()) {
      window.alert("Data Saved");
      setIsDisable(true);
      dispatch(updateUserById(user, values));
    } else {
      console.log("Not submit");
    }
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const Input = styled("input")({
    display: "none",
  });


  return (
    <Form onSubmit={handleSubmit}>
      <Card>
        <CardHeader title="User Profile" />

        <Divider />
        <AccountProfile
          name={values?.firstName + ' ' + values?.lastName}
          profile={values?.profile}
        />
        <CardContent>
          <Grid container spacing={3}>
            <Grid item md={6} xs={12}>
              <Controls.Input
                label="First name"
                name="firstName"
                onChange={handleChange}
                value={values?.firstName}
                error={errors?.firstName}
                fullWidth
                required
                disabled={isDisable}
              />
            </Grid>
            <Grid item md={6} xs={12}>
              <Controls.Input
                label="Last name"
                name="lastName"
                onChange={handleChange}
                value={values?.lastName}
                error={errors?.lastName}
                fullWidth
                required
                disabled={isDisable}
              />
            </Grid>
            <Grid item md={6} xs={12}>
              <Controls.Input
                label="Designation"
                name="designation"
                onChange={handleChange}
                value={values?.designation}
                error={errors?.designation}
                fullWidth
                required
                disabled={isDisable}
              />
            </Grid>
            <Grid item md={6} xs={12}>
              <Controls.Input
                label="Experience"
                name="experience"
                onChange={handleChange}
                value={values?.experience}
                error={errors?.experience}
                fullWidth
                required
                disabled={isDisable}
              />
            </Grid>
            <Grid item md={6} xs={12}>
              <Controls.Input
                label="Email Address"
                name="email"
                onChange={handleChange}
                value={values?.email}
                error={errors?.email}
                fullWidth
                required
                disabled={isDisable}
              />
            </Grid>

            <Grid item md={6} xs={12}>
              <Controls.Input
                label="Phone Number"
                name="phone"
                onChange={handleChange}
                value={values?.phone}
                error={errors?.phone}
                fullWidth
                required
                disabled={isDisable}
              />
            </Grid>

            <Grid item md={6} xs={12}>
              <Controls.Input
                label="Location"
                name="location"
                onChange={handleChange}
                value={values?.location}
                error={errors?.location}
                fullWidth
                required
                disabled={isDisable}
              />
            </Grid>
            <Grid item md={6} xs={12}>
              <Controls.Input
                label="Status"
                name="status"
                onChange={handleChange}
                value={values?.status}
                error={errors?.status}
                fullWidth
                required
                disabled={isDisable}
              />
            </Grid>
            <Grid item xs={12}>
              <SkillSet
                skills={values?.skills}
                isReadOnly={isDisable}
                handleAddition={(tag) => handleAddition(tag)}
                handleDelete={(i) => handleDelete(i)}
                handleDrag={(tag, currPos, newPos) =>
                  handleDrag(tag, currPos, newPos)
                }
              />
            </Grid>
            <Grid item xs={12}>
              <Paper
                sx={{
                  display: "flex",
                  justifyContent: "center",
                  flexWrap: "wrap",
                  listStyle: "none",
                  width: "750px",
                  p: 0.5,
                  m: 0,
                }}
                component="ul"
              >
                <label htmlFor="contained-button-file">
                  <Input
                    accept=".pdf"
                    id="contained-button-file"
                    multiple
                    type="file"
                    onChange={onChange}
                  />
                  <Button
                    size="large"
                    variant="contained"
                    component="span"
                    disabled={isDisable}
                  >
                    Upload Profile <FileUploadIcon />
                  </Button>
                </label>
                {url && (
                  <>
                    <Grid
                      container
                      justifyContent="center"
                      alignItems="center"
                      className="m-2"
                    >
                      <h5 className="mx-2">{fileName}</h5>
                      <Button
                        size="large"
                        variant="contained"
                        component="span"
                        disabled={isDisable}
                        onClick={handleClickOpen}
                      >
                        View Profile <PictureAsPdfIcon />
                      </Button>
                      <PreviewPdf
                        url={url}
                        open={open}
                        handleClose={handleClose}
                      />
                    </Grid>
                  </>
                )}
              </Paper>
            </Grid>
          </Grid>
        </CardContent>
        <Divider />
        <ActionButtons isError={isError} isDisable={isDisable} setIsDisable={setIsDisable} />
      </Card>
    </Form>
  );
};
