import React, { useEffect } from "react";
import {
  Avatar,
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  IconButton,
  ListItemAvatar,
  Paper,
  TextField,
  Typography,
} from "@mui/material";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import MuiListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import { useSelector, useDispatch } from "react-redux";
import Autocomplete from "@mui/material/Autocomplete";
import { styled } from "@mui/material/styles";
import "./Profile.css";
import { fetchUsers } from "../../../redux/actions/userAction";

const ListItem = withStyles({
  root: {
    "&$selected": {
      backgroundColor: "#ACACE6",
      color: "black",
      "& .MuiListItemIcon-root": {
        color: "black",
      },
    },
    "&$selected:hover": {
      backgroundColor: "purple",
      color: "white",
      "& .MuiListItemIcon-root": {
        color: "white",
      },
    },
    "&:hover": {
      backgroundColor: "#e8f0fe",
      color: "black",
      "& .MuiListItemIcon-root": {
        color: "white",
      },
    },
  },
  selected: {},
})(MuiListItem);

export const Otherprofiles = ({ selectedUser }) => {
  const { users: employeeData, loading } = useSelector((state) => state.user);
  const dispatch = useDispatch();
  const [selectedIndex, setSelectedIndex] = React.useState(0);
  const handleListItemClick = (event, index, id) => {
    setSelectedIndex(index);
    selectedUser(id);
  };
  useEffect(() => {
    dispatch(fetchUsers());
  }, []);
  return (
    <Card>
      <Autocomplete
        freeSolo
        id="searchbar"
        className="m-2"
        disableClearable
        options={employeeData.map((employee) => employee.firstName)}
        renderInput={(params) => (
          <TextField
            {...params}
            label="Search input"
            InputProps={{
              ...params.InputProps,
              type: "search",
            }}
          />
        )}
      />
      {employeeData.map((employee, index) => (
        <div>
          <List component="nav">
            <ListItem
              selected={selectedIndex === index}
              onClick={(event) =>
                handleListItemClick(event, index, employee.id)
              }
            >
              <ListItemAvatar>
                <Avatar src={employee.profile} />
              </ListItemAvatar>

              <ListItemText
                className="m-2"
                primary={employee?.firstName}
                secondary={employee?.designation}
              />
            </ListItem>
            <Divider />
          </List>
        </div>
      ))}

      {/* <Grid
      container
      spacing={2}
      direction="column"
      justifyContent="center"
      alignItems="center"
    >
      {Employeedata.map((user) => (
        <>
          {" "}
          <Grid item xs={12} className="mt-3">
            <Item>
              <Avatar src={user.profile} />
              <Typography
                className="mx-auto"
                color="textPrimary"
                gutterBottom
                variant="h6"
              >
                {user.name}
              </Typography>
            </Item>
          </Grid>
         
        </>
      ))}
    </Grid> */}
    </Card>
  );
};
