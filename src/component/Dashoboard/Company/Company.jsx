import React, { useState } from "react";
import { styled } from "@mui/material/styles";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import CompanyDetails from "./CompanyDetails";
import {  Card, Container, Divider } from "@mui/material";
function Company() {
  return (
    <>
      <Card
        component="main"
        sx={{
          flexGrow: 1,
          py: 1,
        }}
      >
        <Container maxWidth="lg" className="p-1">
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <CompanyDetails  />
            </Grid>
          </Grid>
        </Container>
      </Card>
   
    </>
  );
}

export default Company;
