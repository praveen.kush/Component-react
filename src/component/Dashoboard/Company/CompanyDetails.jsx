import React, { useEffect, useState } from "react";
import Grid from "@mui/material/Grid";
import company from "../../../Assets/company.jpg";
import {
  Card,
  CardContent,
  CardHeader,
  Divider,
  TextField,
} from "@mui/material";
import SkillSet from "../../Shared/SkillSet/SkillSet";
import AccountProfile from "../../Shared/AccountProfile";
import { useForm, Form } from "../../custom hooks/useForm";
import ActionButtons from "../../Shared/ActionButtons";
import CompanyImages from "./CompanyImages";
import YoutubeEmbed from "./YoutubeEmbed";
import MyMapComponent from "./CompanyLocation";
import Controls from "../../controls/Controls";
import CompanyData from "../../CompanyData";

function CompanyDetails() {
  const validate = (fieldValues = values) => {
    console.log(fieldValues)
    let temp = { ...errors };
    if ("companyName" in fieldValues)
      temp.companyName = fieldValues.companyName
        ? ""
        : "This field is required.";
    if ("foundation" in fieldValues)
      temp.foundation = fieldValues.foundation ? "" : "This field is required.";
    if ("regName" in fieldValues)
      temp.regName = fieldValues.regName ? "" : "This field is required.";
    if ("managerName" in fieldValues)
      temp.managerName = fieldValues.managerName
        ? ""
        : "This field is required.";
    if ("strength" in fieldValues)
      temp.strength = fieldValues.strength ? "" : "This field is required.";
    if ("location" in fieldValues)
      temp.location = fieldValues.location ? "" : "This field is required.";

    if ("companyEmail" in fieldValues)
      temp.companyEmail = /$^|.+@.+..+/.test(fieldValues.companyEmail)
        ? fieldValues.companyEmail ? '':'Email is required'
        : "Email is not valid.";
   
    if ("contact" in fieldValues)
      temp.contact =
        fieldValues.contact.toString().length > 9
          ? ""
          : "Minimum 10 numbers required.";
    setErrors({
      ...temp,
    });

    if (fieldValues == values) return Object.values(temp).every((x) => x == "");
  };
  const {
    values,
    setValues,
    errors,
    setErrors,
    handleChange,
    isDisable,
    setIsDisable,
    resetForm,
  } = useForm(true, validate);
  useEffect(() => {
    setValues(CompanyData[0]);
  }, []);
  const handleSubmit = (e) => {
    e.preventDefault();
    if (validate()) {
      window.alert("Data Saved");
    } else {
      console.log("Not submit");
    }
  };

  return (
    <Form onSubmit={handleSubmit}>
      <Card>
        <CardHeader title="Company Profile" />
        <Divider />
        <CardContent>
          <AccountProfile name={values?.companyName} profile={company} />

          <Grid container spacing={3} sx={{ marginLeft: 3 }}>
            <Grid item md={6} xs={12}>
              <Controls.Input
                label="Company Name"
                name="companyName"
                value={values?.companyName}
                onChange={handleChange}
                error={errors?.companyName}
                fullWidth
                required
                disabled={isDisable}
              />
            </Grid>
            <Grid item md={6} xs={12}>
              <Controls.Input
                label="Foundation Year"
                name="foundation"
                value={values?.foundation}
                error={errors?.foundation}
                onChange={handleChange}
                fullWidth
                required
                disabled={isDisable}
              />
            </Grid>
            <Grid item md={6} xs={12}>
              <Controls.Input
                label="Registered Company Name"
                name="regName"
                value={values?.regName}
                error={errors?.regName}
                onChange={handleChange}
                fullWidth
                required
                disabled={isDisable}
              />
            </Grid>
            <Grid item xs={6}>
              <Controls.Input
                label="Company Email"
                name="companyEmail"
                value={values?.companyEmail}
                error={errors?.companyEmail}
                onChange={handleChange}
                fullWidth
                required
                disabled={isDisable}
              />
            </Grid>
            <Grid item xs={6}>
              <Controls.Input
                label="Company Strength"
                name="strength"
                value={values?.strength}
                error={errors?.strength}
                onChange={handleChange}
                fullWidth
                required
                disabled={isDisable}
              />
            </Grid>
            <Grid item xs={6}>
              <Controls.Input
                label="Location"
                name="location"
                value={values?.location}
                error={errors?.location}
                onChange={handleChange}
                fullWidth
                required
                disabled={isDisable}
              />
            </Grid>
            <Grid item xs={12}>
              <SkillSet editable={isDisable} />
            </Grid>
            <Grid item xs={6}>
              <Controls.Input
                label="HR Manager"
                name="managerName"
                value={values?.managerName}
                error={errors?.managerName}
                onChange={handleChange}
                fullWidth
                required
                disabled={isDisable}
              />
            </Grid>
            <Grid item xs={6}>
              <Controls.Input
                label="HR Contact"
                name="contact"
                value={values?.contact}
                error={errors?.contact}
                onChange={handleChange}
                fullWidth
                required
                disabled={isDisable}
              />
            </Grid>
            <Grid item xs={6}>
              <CompanyImages />
            </Grid>
            <Grid item xs={6}>
              <YoutubeEmbed embedId="q5p3sP2rEOM" />
              <MyMapComponent
                isMarkerShown
                googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places"
                loadingElement={<div style={{ height: `100%` }} />}
                containerElement={<div style={{ height: `200px` }} />}
                mapElement={<div style={{ height: `100%` }} />}
              />
            </Grid>
          </Grid>
        </CardContent>
        <Divider />
        <ActionButtons setIsDisable={setIsDisable} />
      </Card>
    </Form>
  );
}

export default CompanyDetails;
