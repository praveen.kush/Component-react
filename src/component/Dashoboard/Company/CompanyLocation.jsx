import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps"

const MyMapComponent = withScriptjs(withGoogleMap((props) =>
  <GoogleMap
    defaultZoom={8}
    defaultCenter={{ lat:23.0385, lng: 72.5129 }}
  >
    {props.isMarkerShown && <Marker position={{ lat:23.0385, lng: 72.5129 }} />}
  </GoogleMap>
))

export default MyMapComponent