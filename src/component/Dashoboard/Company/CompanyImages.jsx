import React from "react";
import ImageList from "@mui/material/ImageList";
import ImageListItem from "@mui/material/ImageListItem";
import ImageListItemBar from "@mui/material/ImageListItemBar";
import IconButton from "@mui/material/IconButton";
import StarBorderIcon from "@mui/icons-material/StarBorder";
function CompanyImages() {
  function srcset(image, width, height, rows = 1, cols = 1) {
    return {
      src: `${image}?w=${width * cols}&h=${height * rows}&fit=crop&auto=format`,
      srcSet: `${image}?w=${width * cols}&h=${
        height * rows
      }&fit=crop&auto=format&dpr=2 2x`,
    };
  }
  return (
    <ImageList
      sx={{
        width: 500,
        height: 460,
        // Promote the list into its own layer in Chrome. This costs memory, but helps keeping high FPS.
        transform: "translateZ(0)",
      }}
      rowHeight={200}
      gap={3}
    >
      {itemData.map((item) => {
        const cols = item.featured ? 2 : 1;
        const rows = item.featured ? 2 : 1;

        return (
          <ImageListItem key={item.img} cols={cols} rows={rows}>
            <img
              {...srcset(item.img, 250, 200, rows, cols)}
              alt={item.title}
              loading="lazy"
            />
            <ImageListItemBar
              sx={{
                background:
                  "linear-gradient(to bottom, rgba(0,0,0,0.7) 0%, " +
                  "rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)",
              }}
              title={item.title}
              position="top"
              actionPosition="left"
            />
          </ImageListItem>
        );
      })}
    </ImageList>
  );
}
const itemData = [
  {
    img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTN-RgLB3l5gMWDcXoBWCplMtJCkToOKjPe9bDkxO6MvW3QoM3PAmxD4Qwro1xpzqN9avk&usqp=CAU",
  },
  {
    img: "https://media.glassdoor.com/lst2x/399855/gateway-technolabs-office.jpg",
  },
  {
    img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTR4XM1xX0mqEHGjQn2OUJLoBoRDrO865LjfThMvJ1kBazb-W3QKu1gzDSo6_IfrN5_PD4&usqp=CAU",
  },
  {
    img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTk_SMHnNFgocWIIbZTmbw_Twx2v6FYqZu1AyM4hfvaQckXhanCLa4oNLYbdXMSdCUgX60&usqp=CAU",
  },
  {
    img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ6lMaIaOqX_kbnMR-lSBkleIdWfxLRPT4v6g&usqp=CAU",
  },
];

export default CompanyImages;
