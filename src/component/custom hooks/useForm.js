import React, { useState } from "react";
import { makeStyles } from "@material-ui/core";

export function useForm(validateOnChange = false, validate) {
  const [values, setValues] = useState();
  const [isError, setIsError] = useState(false);
  const [errors, setErrors] = useState({});
  const [isDisable, setIsDisable] = useState(true);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setValues({
      ...values,
      [name]: value,
    });
    if (validateOnChange) validate({ [name]: value });
  };
  const handleDelete = (i) => {
    let remainingTags = values?.skills.filter((skill, index) => index !== i);
    setValues({ ...values, skills: remainingTags });
  };

  const handleAddition = (tag) => {
    let newTag = { ...values, skills: [...values.skills, tag] };
    setValues(newTag);
   
  };
  const handleDrag = (tag, currPos, newPos) => {
    const newTags = values?.skills?.slice();
    newTags.splice(currPos, 1);
    newTags.splice(newPos, 0, tag);
    //re-render
    setValues({ ...values, skills: newTags });
  };

  return {
    values,
    setValues,
    isError,
    setIsError,
    errors,
    setErrors,
    handleChange,
    isDisable,
    setIsDisable,
    handleAddition,
    handleDrag,
    handleDelete,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiFormControl-root": {
      width: "80%",
      margin: theme.spacing(1),
    },
  },
}));

export function Form(props) {
  const classes = useStyles();
  const { children, ...other } = props;
  return (
    <form className={classes.root} autoComplete="off" {...other}>
      {props.children}
    </form>
  );
}
