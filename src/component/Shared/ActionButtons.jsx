import { Box, Button } from "@mui/material";
import React from "react";
function ActionButtons({ isError,isDisable, setIsDisable }) {
  return (
    <>
      <Box
        sx={{
          display: "flex",
          justifyContent: "flex-end",
          p: 2,
        }}
      >
        <Button
          color="primary"
          variant="contained"
          className="me-2"
          onClick={() => {
            setIsDisable(false);
          }}
        >
          Edit details
        </Button>
        <Button
          type="submit"
          color="primary"
          variant="contained"
          disabled={isError || isDisable}
          // onClick={() => {
          //   setIsDisable(true);
          // }}
        >
          Save details
        </Button>
      </Box>
    </>
  );
}

export default ActionButtons;
