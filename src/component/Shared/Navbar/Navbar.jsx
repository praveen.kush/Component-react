import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import "./Navbar.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBuffer } from "@fortawesome/free-brands-svg-icons";
import { useState } from "react";
import { useEffect } from "react";
import { Container, Nav, Navbar } from "react-bootstrap";
import IconButton from "@mui/material/IconButton";
import Brightness4Icon from "@mui/icons-material/Brightness4";
import Brightness7Icon from "@mui/icons-material/Brightness7";
import PopOver from "../PopOver/PopOver";
import { UserContext } from "../../../App";
import { toggleDarkMode } from "../../../redux/actions/themeAction";

const NavBar = () => {
  const { user, handleLoginModal } = useContext(UserContext);
  const [isSticky, setSticky] = useState(false);
  const { theme } = useSelector((state) => state.theme);
  const dispatch = useDispatch();

  useEffect(() => {
    window.addEventListener("scroll", () => {
      if (window.scrollY > 50) {
        setSticky(true);
      } else {
        setSticky(false);
      }
    });
  }, []);
  const toggleMode = (theme) => {
    dispatch(toggleDarkMode(theme));
  };
  const scrollTop = () => window["scrollTo"]({ top: 0, behavior: "smooth" });
  return (
    <Navbar
      className={`navbar navbar-expand-lg  ${
        isSticky ? "navStyle" : "navDefault"
      }`}
      expand="lg"
    >
      <Container>
        <Navbar.Brand as={Link} to="/" onClick={scrollTop} className="navBrn">
          <FontAwesomeIcon icon={faBuffer} className="brnIcon" />{" "}
          <span className="navNormal">Easy</span>{" "}
          <span className="navHighlight">Consulting</span>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto mainNav" activeKey="/home">
            <Nav.Item>
              <Nav.Link
                as={Link}
                to="/"
                className="nav-link"
                onClick={() =>
                  window["scrollTo"]({ top: 0, behavior: "smooth" })
                }
              >
                Home
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link href="#services" className="nav-link">
                Services
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link href="#testimonial" className="nav-link">
                Reviews
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link href="#contact" className="nav-link">
                Contact
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link as={Link} to="/dashboard/profile" className="nav-link">
                Dashboard
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link as={Link} to="/employee" className="nav-link">
                Employee
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <div className="p-2">
                {theme === "light" ? (
                  <IconButton
                    onClick={() => {
                      toggleMode("dark");
                    }}
                    color="inherit"
                  >
                    <Brightness4Icon />
                  </IconButton>
                ) : (
                  <IconButton
                    onClick={() => {
                      toggleMode("light");
                    }}
                    color="inherit"
                  >
                    <Brightness7Icon />
                  </IconButton>
                )}
              </div>
            </Nav.Item>
            <Nav.Item>
              {user.email ? (
                <div>
                  <PopOver />
                </div>
              ) : (
                <Link to="/">
                  <button className="loginBtn" onClick={handleLoginModal}>
                    Login
                  </button>
                </Link>
              )}
            </Nav.Item>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default NavBar;
