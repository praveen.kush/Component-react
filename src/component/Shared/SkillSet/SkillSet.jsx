import React from "react";
import Paper from "@mui/material/Paper";
import { WithContext as ReactTags } from "react-tag-input";
import "./SkillSet.css";

const technologies = [
  "Javascript",
  "C++",
  "C#",
  "Node.js",
  "Swift",
  "AngularJS",
  "Azure",
  ".Net",
  "Java",
  "AWS",
];
const suggestions = technologies.map((tech) => {
  return {
    id: tech,
    text: tech,
  };
});

const KeyCodes = {
  comma: 188,
  enter: 13,
};

const delimiters = [KeyCodes.comma, KeyCodes.enter];

const SkillSet = ({
  skills,
  isReadOnly,
  handleAddition,
  handleDelete,
  handleDrag,
}) => {
  console.log(isReadOnly);
  return (
    <Paper
      sx={{
        display: "flex",
        justifyContent: "center",
        flexWrap: "wrap",
        listStyle: "none",
        p: 0.5,
        m: 0,
      }}
      component="ul"
    >
      <div className="app">
        <h4>{!isReadOnly ? "Add Skills" : "Skills Provided"} </h4>
        <div>
          {" "}
          {isReadOnly ? (
            <ReactTags
              tags={skills}
              suggestions={suggestions}
              delimiters={delimiters}
              handleDelete={handleDelete}
              handleAddition={handleAddition}
              handleDrag={handleDrag}
              removeOnBackspace={false}
              inputFieldPosition="bottom"
              placeholder="Enter your area of expertise/specialization"
              autocomplete
              readOnly
            />
          ) : (
            <>
              <ReactTags
                tags={skills}
                suggestions={suggestions}
                delimiters={delimiters}
                handleDelete={handleDelete}
                handleAddition={handleAddition}
                handleDrag={handleDrag}
                removeOnBackspace={false}
                inputFieldPosition="bottom"
                placeholder="Enter your area of expertise/specialization"
                autocomplete
              />
            </>
          )}
        </div>
      </div>
    </Paper>
  );
};

export default SkillSet;
