import React, {useState, useEffect} from "react";
import { CardContent, Typography , Box} from "@mui/material";
import ImageAvtar from "./ImageAvtar";
import ImageCrop from './ImageCrop'
function AccountProfile({name, profile}) {
    const [upImg, setUpImg] = useState();
    const [showModal, setShowModal] = useState(false);
    const onSelectFile = (e) => {
      if (e.target.files && e.target.files.length > 0) {
        const reader = new FileReader();
        reader.addEventListener("load", () => {
          setUpImg(reader.result);
          setShowModal(true);
        });
        reader.readAsDataURL(e.target.files[0]);
      }
    };
  
    useEffect(() => {
      setUpImg(profile);
    }, [profile]);
  return (
    <>
      {" "}
      <CardContent>
        <Box
          sx={{
            alignItems: "center",
            display: "flex",
            flexDirection: "column",
          }}
        >
          <ImageAvtar upImg={upImg} onSelectFile={onSelectFile} />
          <ImageCrop
            img={upImg}
            showModal={showModal}
            setShowModal={setShowModal}
            setUpImg={setUpImg}
          />

          <Typography color="textPrimary" gutterBottom variant="h5">
            {name}
          </Typography>
        </Box>
      </CardContent>
    </>
  );
}

export default AccountProfile;
