import React, { useState, useCallback, useRef } from "react";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogTitle from "@mui/material/DialogTitle";
import ReactCrop from "react-image-crop";
import { Button, DialogContent } from "@mui/material";
import 'react-image-crop/dist/ReactCrop.css';

function ProfileUploadAndCrop(props) {
  const { img, setUpImg, showModal, setShowModal } = props;
  const imgRef = useRef(null);
  const [crop, setCrop] = useState({ unit: "%", width: 30, aspect: 16 / 16 });
  const [completedCrop, setCompletedCrop] = useState(null);
  const handleClose = () => {
    setShowModal(false);
  };

  const onLoad = useCallback((img) => {
    imgRef.current = img;
  }, []);
  const getCroppedImg = () => {
    const image = imgRef.current;
    const canvas = document.createElement("canvas");
    const crop = completedCrop;

    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    const ctx = canvas.getContext("2d");
    const pixelRatio = window.devicePixelRatio;

    canvas.width = crop.width * pixelRatio * scaleX;
    canvas.height = crop.height * pixelRatio * scaleY;

    ctx.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0);
    ctx.imageSmoothingQuality = "high";

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width * scaleX,
      crop.height * scaleY
    );
    const base64Image = canvas.toDataURL("image/jpeg", 1);
    setUpImg(base64Image);
    setShowModal(false);
  };
  return (
    <>
      <Dialog
        open={showModal}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
 
      >
        <DialogTitle id="alert-dialog-title">Crop Image</DialogTitle>

        <DialogContent sx={{height:'350px' , width:'400px'}}>
          <ReactCrop
            src={img}
            onImageLoaded={onLoad}
            imageStyle={{height:'320px', width:'350px'}}
            circularCrop 
            crop={crop}
            onChange={(c) => setCrop(c)}
            onComplete={(c) => setCompletedCrop(c)}
          />
         
        </DialogContent>
        <DialogActions>
            <Button onClick={getCroppedImg}>Confirm</Button>
          </DialogActions>
      </Dialog>
    </>
  );
}

export default ProfileUploadAndCrop;
