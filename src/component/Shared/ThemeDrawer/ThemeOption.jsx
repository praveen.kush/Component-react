import * as React from "react";
import { useSelector, useDispatch } from "react-redux";
import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import { toggleDarkMode } from "../../../redux/actions/themeAction";

export default function ThemeOption() {
  const { theme } = useSelector((state) => state.theme);
  const dispatch = useDispatch();
  const toggleMode = (theme) => {
    dispatch(()=>{toggleDarkMode(theme)});
  };
  return (
    <FormControl>
      <FormLabel id="demo-row-radio-buttons-group-label">Theme</FormLabel>
      <RadioGroup
        row
        aria-labelledby="demo-row-radio-buttons-group-label"
        name="row-radio-buttons-group"
        defaultValue={theme}
      >
        <FormControlLabel
          value="light"
          control={<Radio />}
          label="Light"
          onChange={() => {
            toggleMode("light");
          }}
        ></FormControlLabel>
        <FormControlLabel
          value="dark"
          control={<Radio />}
          label="Dark"
          onChange={() => {
            toggleMode("dark");
          }}
        />
      </RadioGroup>
    </FormControl>
  );
}
