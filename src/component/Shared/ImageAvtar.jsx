import React from "react";
import { faCamera } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
function ImageAvtar({upImg, onSelectFile }) {
  return (
    <>
      <div class="container">
        <div class="avatar">
          <img className="avatar" src={upImg} />
          <label htmlFor="file">
            <FontAwesomeIcon className="edit" icon={faCamera} />
          </label>
          <input
            id="file"
            type="file"
            accept="image/*"
            onChange={(e)=>onSelectFile(e)}
            style={{ display: "none" }}
          />
        </div>
      </div>
    </>
  );
}

export default ImageAvtar;
